const mongoose = require('mongoose');

const deviceSchema = mongoose.Schema({
    IMEI1: Number,
    IMEI2: Number,
    ip:String,
    macAdd:String,
    serialNumber:String,
    company: String,
    is_defective:Boolean
});

const devices = mongoose.model('devices', deviceSchema);

module.exports = devices;