const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/devices', { useNewUrlParser: true }, () => {
    console.log('DB connected!')
});
module.exports = mongoose