// Import Required Packages
const express = require('express');
const app = express();

// Sever Port Init
const port = 8080;

// Require Database Config
require('./config/config');
const { getData } = require('./data')
const Devices = require('./config/deviceModel')

//  Save data  Route
app.get('/save', (req, res) => {
    const devices = new Devices(getData())
    devices.save((err, data) => {
        if (err) {
            console.log('Error', err)
        }
    })

    res.status(200).send('data saved!')
})

//Get data route

app.get('/devices:counts', (req, res) => {
    let num = parseInt(req.params.counts)
    Devices.find({}).limit(num).then((data, err) => {
        if (req.params.counts <= 0) {
            res.send('Sorry we have no record!')
        } else if (req.params.counts > data.length) {
            res.send(`Sorry we have only ${data.length} records`)
        }
        else if (err) {
            console.log('Error', err)
        } else
            res.send(data)
    })
})


// Get all devices

app.get('/loaddevices', (req, res) => {
    Devices.find({}).then((data, err) => {
       if (err) {
            console.log('Error', err)
        } else
            res.send(data)
    })
})

// starting server
app.listen(port, () => {
    console.log(`server is listiong at port ${port}`)
})