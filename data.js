module.exports = {

    getData: () => {
        return {
            IMEI1: Math.round(Math.random() * 10000 * 9000000),
            IMEI2: Math.round(Math.random() * 10000 * 9000000),
            ip: `${Math.round(Math.random() * 1 * 99)}.${Math.round(Math.random() * 1 * 99)}.${Math.round(Math.random() * 1 * 99)}.${Math.round(Math.random() * 1 * 99)}`,
            macAdd: `${Math.round(Math.random() * 1 * 99)}:${Math.round(Math.random() * 1 * 99)}:${Math.round(Math.random() * 1 * 99)}:${Math.round(Math.random() * 1 * 99)}:${Math.round(Math.random() * 1 * 99)}:${Math.round(Math.random() * 1 * 99)}`,
            serialNumber: `${Math.random().toString(36).substr(2, 100)}${Math.random().toString(36).substr(2, 100)}`,
            company: `${['Nokia', 'Sumsing', 'Oppo', 'LG', 'Huawei', 'Qmobile'][Math.floor(Math.random() * 3)]}`,
            is_defective:false
        }
    }

}